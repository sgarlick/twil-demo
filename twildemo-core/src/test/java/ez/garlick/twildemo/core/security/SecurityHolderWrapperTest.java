package ez.garlick.twildemo.core.security;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

import org.junit.Before;
import org.junit.Test;
import org.springframework.security.authentication.TestingAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.context.SecurityContextImpl;

import ez.garlick.twildemo.core.security.SecurityHolderWrapper;

public class SecurityHolderWrapperTest {

	private SecurityHolderWrapper securityHolderWrapper;
	private Authentication authentication;

	@Before
	public void setup() {
		SecurityContext context = new SecurityContextImpl();
		authentication = new TestingAuthenticationToken("admin", "admin");
		context.setAuthentication(authentication);
		SecurityContextHolder.setContext(context);
		securityHolderWrapper = new SecurityHolderWrapper();
	}

	@Test
	public void getPrincipal() {
		assertThat("getPrincipal should return admin", "admin",
				is(securityHolderWrapper.getPrincipal()));
	}

	@Test
	public void getAuthentication() {
		assertThat(
				"getAuthentication should return Authentication object set on SecurityContextHolder",
				authentication, is(securityHolderWrapper.getAuthentication()));
	}

	@Test
	public void getName() {

		assertThat("getNmae should return admin", "admin",
				is(securityHolderWrapper.getAuthentication().getName()));
	}
}
