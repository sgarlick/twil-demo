package ez.garlick.twildemo.core.security;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.beans.factory.BeanInitializationException;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcOperations;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;

@RunWith(MockitoJUnitRunner.class)
public class CapabilityUserDetailServiceTest {
	@Mock
	private NamedParameterJdbcOperations namedParameterJdbcOperations;
	@Mock
	private UserDetailsService userDetailsService;

	@InjectMocks
	private CapabilityUserDetailService service;

	@Before
	public void setup() {
	}

	@Test
	@SuppressWarnings(value = "unchecked")
	public void loadUserByUsername() {
		String username = "admin";
		Collection<GrantedAuthority> grantedAuthorities = new HashSet<GrantedAuthority>();
		GrantedAuthority authority = new SimpleGrantedAuthority("ROLE_TEST");
		grantedAuthorities.add(authority);
		UserDetails userDetails = new User(username, "admin",
				grantedAuthorities);

		Set<String> roles = new HashSet<String>();
		roles.add(authority.getAuthority());
		Map<String, Collection<String>> params = new HashMap<String, Collection<String>>();
		params.put("roles", roles);

		List<GrantedAuthority> toReturn = new ArrayList<GrantedAuthority>();
		GrantedAuthority cap = new SimpleGrantedAuthority("CAP_TEST");
		toReturn.add(cap);

		when(userDetailsService.loadUserByUsername(username)).thenReturn(
				userDetails);

		when(
				namedParameterJdbcOperations.query(anyString(), any(Map.class),
						any(CapabilityUserDetailService.getCapRowMapper()
								.getClass()))).thenReturn(toReturn);

		UserDetails user = service.loadUserByUsername(username);
		Assert.assertTrue(user.getAuthorities().contains(cap));
	}

	@Test(expected = BeanInitializationException.class)
	public void afterPropertiesSetJDBCOperations() throws Exception {
		service.setNamedParameterJdbcOperations(null);
		service.afterPropertiesSet();
	}

	@Test(expected = BeanInitializationException.class)
	public void afterPropertiesSetDelegating() throws Exception {
		service.setDelegatingUserDetailService(null);
		service.afterPropertiesSet();

	}
}
