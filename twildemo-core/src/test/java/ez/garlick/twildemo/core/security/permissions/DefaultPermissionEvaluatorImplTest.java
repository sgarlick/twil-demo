package ez.garlick.twildemo.core.security.permissions;

import static org.mockito.Mockito.when;

import java.util.HashMap;
import java.util.Map;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.security.authentication.TestingAuthenticationToken;
import org.springframework.security.core.Authentication;

import ez.garlick.twildemo.core.security.permissions.DefaultPermissionEvaluatorImpl;
import ez.garlick.twildemo.core.security.permissions.Permission;
import ez.garlick.twildemo.core.security.permissions.PermissionNotDefinedException;

@RunWith(MockitoJUnitRunner.class)
public class DefaultPermissionEvaluatorImplTest {

	private DefaultPermissionEvaluatorImpl impl;

	@Mock
	private Permission testPermission;
	private Authentication auth;

	@Before
	public void setup() {
		Map<String, Permission> permissions = new HashMap<String, Permission>();
		permissions.put("test", testPermission);
		impl = new DefaultPermissionEvaluatorImpl(permissions);
		auth = new TestingAuthenticationToken("admin", "admin");
	}

	@Test
	public void hasPermissionCantHandle() {
		Assert.assertFalse(impl.hasPermission(null, null, null));
	}

	@Test
	public void hasPermissionNotAllowed() {
		Object object = new Object();
		when(testPermission.isAllowed(auth, object)).thenReturn(false);
		Assert.assertFalse(impl.hasPermission(auth, object, "test"));
	}

	@Test
	public void hasPermission() {
		Object object = new Object();
		when(testPermission.isAllowed(auth, object)).thenReturn(true);
		Assert.assertTrue(impl.hasPermission(auth, object, "test"));
	}

	@Test(expected = PermissionNotDefinedException.class)
	public void hasPermissionUndefinedPermission() {
		impl.hasPermission(auth, new Object(), "derp");
	}

	@Test(expected = PermissionNotDefinedException.class)
	public void hasPermissionUnsupported() {
		impl.hasPermission(null, null, null, null);
	}
}
