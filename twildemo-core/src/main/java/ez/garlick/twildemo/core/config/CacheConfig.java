package ez.garlick.twildemo.core.config;

import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.CachingConfigurer;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.ehcache.EhCacheCacheManager;
import org.springframework.cache.ehcache.EhCacheFactoryBean;
import org.springframework.cache.ehcache.EhCacheManagerFactoryBean;
import org.springframework.cache.interceptor.DefaultKeyGenerator;
import org.springframework.cache.interceptor.KeyGenerator;
import org.springframework.context.annotation.AdviceMode;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.security.core.userdetails.UserCache;
import org.springframework.security.core.userdetails.cache.EhCacheBasedUserCache;

@Configuration
@EnableCaching(mode = AdviceMode.ASPECTJ)
public class CacheConfig implements CachingConfigurer {

	/**
	 * ehCacheManager that loads our xml config
	 * 
	 * @return
	 */
	@Bean
	public EhCacheManagerFactoryBean ehCacheManager() {
		EhCacheManagerFactoryBean bean = new EhCacheManagerFactoryBean();
		bean.setConfigLocation(new ClassPathResource("ehcache.xml"));
		return bean;
	}

	/**
	 * This is required for spring cache abstraction
	 * 
	 * @return
	 */
	@Override
	@Bean
	public CacheManager cacheManager() {
		EhCacheCacheManager manager = new EhCacheCacheManager();
		manager.setCacheManager(ehCacheManager().getObject());
		return manager;
	}

	/**
	 * This bean creates the userCache
	 * 
	 * @return
	 */
	@Bean
	public EhCacheFactoryBean userEhCache() {
		EhCacheFactoryBean bean = new EhCacheFactoryBean();
		bean.setCacheName("userCache");
		bean.setCacheManager(ehCacheManager().getObject());
		return bean;
	}

	/**
	 * This is a user cache used in the digestFilter bean to cache user info for
	 * web services.
	 * 
	 * @return
	 */
	@Bean
	public UserCache userCache() {
		EhCacheBasedUserCache cache = new EhCacheBasedUserCache();
		cache.setCache(userEhCache().getObject());
		return cache;
	}

	@Override
	@Bean
	public KeyGenerator keyGenerator() {
		return new DefaultKeyGenerator();
	}
}
