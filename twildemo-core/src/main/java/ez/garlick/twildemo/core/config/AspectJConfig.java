package ez.garlick.twildemo.core.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

/**
 * This class is used to configure any AOP beans
 * 
 * @author stephen.garlick
 * 
 */
@Configuration
@EnableAspectJAutoProxy
public class AspectJConfig {

}
