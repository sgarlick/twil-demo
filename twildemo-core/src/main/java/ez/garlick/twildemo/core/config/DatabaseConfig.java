package ez.garlick.twildemo.core.config;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.sql.DataSource;

import org.apache.commons.configuration.ConfigurationConverter;
import org.apache.commons.configuration.DatabaseConfiguration;
import org.springframework.beans.factory.config.PropertyPlaceholderConfigurer;
import org.springframework.context.annotation.AdviceMode;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * This is the main configuration for our database resources.
 * 
 * @author stephen.garlick
 * 
 */
@Configuration
@EnableTransactionManagement(mode = AdviceMode.ASPECTJ)
@ComponentScan(basePackages = "ez.garlick.twildemo.core.dao")
public class DatabaseConfig {

	/**
	 * {@link DataSource} to be looked up in our application container
	 * 
	 * @return
	 * @throws Exception
	 */
	@Bean
	public static DataSource dataSource() throws Exception {
		Context ctx = new InitialContext();
		return (DataSource) ctx.lookup("java:comp/env/jdbc/datasource");
	}

	/**
	 * Our transaction manager
	 * 
	 * @return
	 * @throws Exception
	 */
	@Bean
	public PlatformTransactionManager txManager() throws Exception {
		return new DataSourceTransactionManager(dataSource());
	}

	/**
	 * Springs main class for accessing the database. This should be the main
	 * bean used to perform database interactions.
	 * 
	 * @return
	 * @throws Exception
	 */
	@Bean
	public NamedParameterJdbcTemplate namedParameterJdbcTemplate()
			throws Exception {
		return new NamedParameterJdbcTemplate(dataSource());
	}

	/**
	 * This bean loads a set of properties from the database table app_config.
	 * 
	 * @return
	 */
	@Bean
	public static PropertyPlaceholderConfigurer properties() throws Exception {
		PropertyPlaceholderConfigurer configurer = new PropertyPlaceholderConfigurer();
		DatabaseConfiguration databaseConfig = new DatabaseConfiguration(
				dataSource(), "app_config", "key", "value");
		configurer.setProperties(ConfigurationConverter
				.getProperties(databaseConfig));
		return configurer;
	}

}
