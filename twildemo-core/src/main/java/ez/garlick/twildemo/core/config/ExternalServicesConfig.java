package ez.garlick.twildemo.core.config;

import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.PoolingClientConnectionManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;
/**
 * This is the main configuration class for external (RESTFul) services
 * @author stephen.garlick
 *
 */
@Configuration
@ComponentScan(basePackages = "ez.garlick.twildemo.core.external")
public class ExternalServicesConfig {
	/**
	 * To load a service information from the database app_config table
	 * and then use that to build the BasicCredentialsProvider in the restTemplate bean and your external service bean
	 * @Value("${external.service.host}") String hostName;
	 * @Value("${external.service.user}") String userName;
	 * @Value("${external.service.pass}") String password;
	 */
	
	
	/**
	 * This is our primary means of calling RESTful web services. It allows
	 * configuration of credentials for multiple host/ports/etc
	 * 
	 * @return
	 */
	@Bean
	public RestTemplate restTemplate() {

		PoolingClientConnectionManager connectionManager = new PoolingClientConnectionManager();
		connectionManager.setDefaultMaxPerRoute(20);
		connectionManager.setMaxTotal(200);

		DefaultHttpClient client = new DefaultHttpClient(connectionManager);
		BasicCredentialsProvider provider = new BasicCredentialsProvider();
		/**
		 * you can configure credentials based on host/port/etc below would
		 * register admin/admin for all calls from this resttemplate
		 * 
		 * provider.setCredentials(AuthScope.ANY, new
		 * UsernamePasswordCredentials("admin", "admin"));
		 * 
		 * consider loading this information as properties from the app_config
		 * table
		 **/
		client.setCredentialsProvider(provider);
		HttpComponentsClientHttpRequestFactory factory = new HttpComponentsClientHttpRequestFactory(
				client);

		return new RestTemplate(factory);
	}
}
