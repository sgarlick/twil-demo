package ez.garlick.twildemo.core.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.twilio.sdk.TwilioRestClient;

@Configuration
public class TwilioConfig {

	@Value("${twilio.accountsid}")
	private String accountSid;
	@Value("${twilio.authtoken}")
	private String authToken;
	
	@Bean
	public TwilioRestClient twilioRestClient()
	{
		return new TwilioRestClient(accountSid, authToken);
	}
}
