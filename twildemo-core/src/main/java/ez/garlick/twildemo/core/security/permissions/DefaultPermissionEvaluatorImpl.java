package ez.garlick.twildemo.core.security.permissions;

import java.io.Serializable;
import java.util.Map;

import org.springframework.security.access.PermissionEvaluator;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

/**
 * Default implmentation of the {@link PermissionEvaluator}. This class provides
 * a simple means to map Strings to {@link Permission} objects for use of the
 * hasPermission expression in spring security
 * 
 * @author stephen.garlick
 * 
 */
@Component
public class DefaultPermissionEvaluatorImpl implements PermissionEvaluator {

	private final Map<String, Permission> permissions;

	public DefaultPermissionEvaluatorImpl(Map<String, Permission> permissions) {
		super();
		this.permissions = permissions;
	}

	@Override
	public boolean hasPermission(Authentication authentication,
			Object targetDomainObject, Object permission) {
		boolean hasPermission = false;
		if (canHandle(authentication, targetDomainObject, permission)) {
			hasPermission = checkPermission(authentication, targetDomainObject,
					(String) permission);
		}
		return hasPermission;
	}

	private boolean canHandle(Authentication authentication,
			Object targetDomainObject, Object permission) {
		return targetDomainObject != null && authentication != null
				&& permission instanceof String;
	}

	private boolean checkPermission(Authentication authentication,
			Object targetDomainObject, String permissionKey) {
		verifyPermissionIsDefined(permissionKey);
		Permission permission = permissions.get(permissionKey);
		return permission.isAllowed(authentication, targetDomainObject);
	}

	private void verifyPermissionIsDefined(String permissionKey) {
		if (!permissions.containsKey(permissionKey)) {
			throw new PermissionNotDefinedException("No permission with key "
					+ permissionKey + " is defined in "
					+ this.getClass().toString());
		}
	}

	@Override
	public boolean hasPermission(Authentication authentication,
			Serializable targetId, String targetType, Object permission) {
		throw new PermissionNotDefinedException(
				"Id and Class permissions are not supperted by "
						+ this.getClass().toString());
	}

	public Map<String, Permission> getPermissions() {
		return permissions;
	}

}
