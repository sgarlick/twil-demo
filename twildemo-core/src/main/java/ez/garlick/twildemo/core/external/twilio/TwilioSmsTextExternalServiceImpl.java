package ez.garlick.twildemo.core.external.twilio;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.springframework.stereotype.Component;

import com.twilio.sdk.TwilioRestClient;
import com.twilio.sdk.TwilioRestException;
import com.twilio.sdk.resource.instance.Account;
import com.twilio.sdk.resource.instance.Sms;

@Component
public class TwilioSmsTextExternalServiceImpl implements TwilioSmsTextExternalService {
	
	@Inject
	private TwilioRestClient client;
	
	@Override
	public String sendSmsText(String message, String to) {
		Account account = client.getAccount();
		Map<String, String> params = new HashMap<String, String>();
	    params.put("Body", message);
	    params.put("To", to);
	    params.put("From", "+17039977959"); //our hardcoded number for easyness, im guessing when you have a lot of numbers you'd just pull a unused one from the api and use that.
	    Sms sms = null;
		try {
			sms = account.getSmsFactory().create(params);
		} catch (TwilioRestException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
		return sms.getSid();
		
	}
	
	@Override
	public List<Sms> fetchSmsTextLog()
	{
		Account account = client.getAccount();
		return account.getSmsMessages().getPageData();
	}
}
