package ez.garlick.twildemo.core.security;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.BeanInitializationException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcOperations;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

/**
 * This class delegates to the injected userDetailsService and then loads
 * capabilities from the database tableName. It is intended to implement the
 * best practice of mapping roles to capabilities.
 * 
 * @author stephen.garlick
 * 
 */
@Component
public class CapabilityUserDetailService implements UserDetailsService {

	private static final Logger logger = LoggerFactory
			.getLogger(CapabilityUserDetailService.class);

	@Resource(name = "userDetailService")
	private UserDetailsService delegatingUserDetailService;

	private String tableName = "auth_role_cap_map";
	private String roleCol = "role";
	private String capCol = "cap";

	@Resource(name = "namedParameterJdbcTemplate")
	private NamedParameterJdbcOperations namedParameterJdbcOperations;

	private static final RowMapper<GrantedAuthority> capRowMapper = new RowMapper<GrantedAuthority>() {
		@Override
		public GrantedAuthority mapRow(ResultSet rs, int rowNum)
				throws SQLException {
			return new SimpleGrantedAuthority(rs.getString(1));
		}
	};

	@Override
	public UserDetails loadUserByUsername(String username)
			throws UsernameNotFoundException {
		UserDetails userDetails = delegatingUserDetailService
				.loadUserByUsername(username);
		return new User(userDetails.getUsername(), userDetails.getPassword(),
				userDetails.isEnabled(), userDetails.isAccountNonExpired(),
				userDetails.isCredentialsNonExpired(),
				userDetails.isAccountNonLocked(),
				loadCapabiltiesAsRoles(userDetails));
	}

	private Collection<GrantedAuthority> loadCapabiltiesAsRoles(
			UserDetails userDetails) {
		Set<GrantedAuthority> roles = new HashSet<GrantedAuthority>(
				userDetails.getAuthorities());

		logger.trace("Loading mapped capabilites for roles: {}", roles);

		if (roles.isEmpty())
			return new ArrayList<GrantedAuthority>();

		Set<GrantedAuthority> capabilities = new HashSet<GrantedAuthority>();
		Map<String, String> searchAuth = new HashMap<String, String>();
		for (GrantedAuthority auth : userDetails.getAuthorities()) {
			searchAuth.clear();
			searchAuth.put("role", auth.getAuthority());
			capabilities.addAll(namedParameterJdbcOperations.query("select "
					+ capCol + " from " + tableName + " where " + roleCol
					+ " = :role", searchAuth, capRowMapper));
		}

		return capabilities;
	}

	@PostConstruct
	public void afterPropertiesSet() throws Exception {
		if (delegatingUserDetailService == null)
			throw new BeanInitializationException(this.getClass().getName()
					+ " must inject a delegatingUserDetailService");
		if (namedParameterJdbcOperations == null)
			throw new BeanInitializationException(this.getClass().getName()
					+ " must inject a namedParameterJdbcOperations");

	}

	public String getTableName() {
		return tableName;
	}

	public String getRoleCol() {
		return roleCol;
	}

	public String getCapCol() {
		return capCol;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	public void setRoleCol(String roleCol) {
		this.roleCol = roleCol;
	}

	public void setCapCol(String capCol) {
		this.capCol = capCol;
	}

	public UserDetailsService getDelegatingUserDetailService() {
		return delegatingUserDetailService;
	}

	public void setDelegatingUserDetailService(
			UserDetailsService delegatingUserDetailService) {
		this.delegatingUserDetailService = delegatingUserDetailService;
	}

	public static RowMapper<GrantedAuthority> getCapRowMapper() {
		return capRowMapper;
	}

	public NamedParameterJdbcOperations getNamedParameterJdbcOperations() {
		return namedParameterJdbcOperations;
	}

	public void setNamedParameterJdbcOperations(
			NamedParameterJdbcOperations namedParameterJdbcOperations) {
		this.namedParameterJdbcOperations = namedParameterJdbcOperations;
	}
}
