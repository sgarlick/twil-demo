package ez.garlick.twildemo.core.config;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Random;

import javax.annotation.Resource;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;
import org.springframework.context.annotation.Profile;
import org.springframework.security.access.PermissionEvaluator;
import org.springframework.security.access.expression.method.DefaultMethodSecurityExpressionHandler;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserCache;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.authentication.DelegatingAuthenticationEntryPoint;
import org.springframework.security.web.authentication.Http403ForbiddenEntryPoint;
import org.springframework.security.web.authentication.LoginUrlAuthenticationEntryPoint;
import org.springframework.security.web.authentication.www.DigestAuthenticationEntryPoint;
import org.springframework.security.web.authentication.www.DigestAuthenticationFilter;
import org.springframework.security.web.util.RequestMatcher;

import ez.garlick.twildemo.core.security.CapabilityUserDetailService;
import ez.garlick.twildemo.core.security.SecurityHolder;
import ez.garlick.twildemo.core.security.SecurityHolderWrapper;
import ez.garlick.twildemo.core.security.permissions.DefaultPermissionEvaluatorImpl;
import ez.garlick.twildemo.core.security.permissions.Permission;

/**
 * This is the main security config for our project. There are three xml files
 * involded. A shared xml for common namespace configuration. And two xml files
 * for to define ldap connecting in the prod env and a embedded ldap server in
 * the dev env.
 * 
 * @author stephen.garlick
 * 
 */
@Configuration
@ImportResource(value = "classpath*:/ez/garlick/twildemo/web/security/application-context-spring-security-common.xml")
public class SecurityConfig {

	/**
	 * This is the userCache we want to use for caching user credentials Defined
	 * in {@link CacheConfig}
	 */
	@Resource(name = "userCache")
	private UserCache userCache;

	/**
	 * This bean simple wraps the static class {@link SecurityContextHolder} and
	 * provides access to security information in an injectable bean
	 * 
	 * @return
	 */
	@Bean
	public SecurityHolder securityHolder() {
		return new SecurityHolderWrapper();
	}

	/**
	 * This is a custom filter used to do digest authentication on RESTFul
	 * webservices. See the common namespace configuration for more
	 * 
	 * @return
	 */
	@Bean
	public DigestAuthenticationFilter digestFilter() {
		DigestAuthenticationFilter filter = new DigestAuthenticationFilter();
		filter.setUserDetailsService(capUserService());
		filter.setAuthenticationEntryPoint(digestEntryPoint());
		filter.setUserCache(userCache);
		return filter;
	}

	/**
	 * This entry point is to supply a digest challenge to our RESTFul
	 * webservices. See the common namespace configuration for more.
	 * 
	 * @return
	 */
	@Bean
	public DigestAuthenticationEntryPoint digestEntryPoint() {
		DigestAuthenticationEntryPoint entryPoint = new DigestAuthenticationEntryPoint();
		entryPoint.setRealmName("Twilio Demo");
		entryPoint.setKey(Integer.toString(new Random().nextInt()));
		return entryPoint;
	}

	/**
	 * This bean is a means to implement the best practice of mapping roles to
	 * capabilites in the database. This class delegates to a
	 * {@link UserDetailsService} and then performs a database look up to add
	 * caps to authorizations. By default this class looks up on the table name
	 * is "auth_role_cap_map" and the role column "role" and cap column "cap". These
	 * defaults can be changed by settings them on this bean.
	 * 
	 * This bean is used in the common namespace to create an authentication
	 * provider.
	 * 
	 * @return
	 */
	@Bean
	public CapabilityUserDetailService capUserService() {
		CapabilityUserDetailService service = new CapabilityUserDetailService();
		return service;
	}
	
	@Resource(name="ajaxRequestMatcher")
	private RequestMatcher ajaxRequestMatcher;

	/**
	 * This entry point will map AjaxRequest to a
	 * {@link Http403ForbiddenEntryPoint} to allow ajax request to receive a 403
	 * status code and other request will be defaulted to a
	 * {@link LoginUrlAuthenticationEntryPoint} This bean is referenced in the
	 * common namespace configuration.
	 * 
	 * @return
	 */
	@Bean
	public DelegatingAuthenticationEntryPoint authEntryPoint() {
		LinkedHashMap<RequestMatcher, AuthenticationEntryPoint> entryPoints = new LinkedHashMap<RequestMatcher, AuthenticationEntryPoint>();
		entryPoints.put(ajaxRequestMatcher,
				new Http403ForbiddenEntryPoint());
		DelegatingAuthenticationEntryPoint authEntryPoint = new DelegatingAuthenticationEntryPoint(
				entryPoints);
		authEntryPoint
				.setDefaultEntryPoint(new LoginUrlAuthenticationEntryPoint(
						"/login"));
		return authEntryPoint;
	}

	/**
	 * This bean is the spring expression handler. We just want to create the
	 * default one and inject our {@link PermissionEvaluator} implementation.
	 * 
	 * This is passed to the global-method-security namespace
	 * 
	 * @return
	 */
	@Bean
	public DefaultMethodSecurityExpressionHandler expressionHandler() {
		DefaultMethodSecurityExpressionHandler handler = new DefaultMethodSecurityExpressionHandler();
		handler.setPermissionEvaluator(permissionEvaluator());
		return handler;
	}

	/**
	 * This bean is a simple implementation of the {@link PermissionEvaluator}
	 * which simple lets us map Strings to {@link Permission} objects. It is
	 * need for use of the hasPermission expression
	 * 
	 * @return
	 */
	@Bean
	public DefaultPermissionEvaluatorImpl permissionEvaluator() {
		Map<String, Permission> permissions = new HashMap<String, Permission>();
		return new DefaultPermissionEvaluatorImpl(permissions);
	}

	/**
	 * This includes our dev env authentication provider bean
	 * 
	 * @author stephen.garlick
	 * 
	 */
	@Configuration
	@Profile("dev")
	@ImportResource(value = "classpath*:/ez/garlick/twildemo/web/security/application-context-spring-security-dev.xml")
	static class DevSecurityConfig {

	}

	/**
	 * This include our prod env authentication provider bean
	 * 
	 * @author stephen.garlick
	 * 
	 */
	@Configuration
	@Profile("prod")
	@ImportResource(value = "classpath*:/ez/garlick/twildemo/web/security/application-context-spring-security.xml")
	static class ProdSecurityConfig {

	}

}
