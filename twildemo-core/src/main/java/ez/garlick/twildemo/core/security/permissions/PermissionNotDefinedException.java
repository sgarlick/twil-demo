package ez.garlick.twildemo.core.security.permissions;

/**
 * An exception that is thrown when an invalid permission is passed into springs
 * hasPermission expression.
 * 
 * @author stephen.garlick
 * 
 */
public class PermissionNotDefinedException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public PermissionNotDefinedException() {
		super();
	}

	public PermissionNotDefinedException(String message) {
		super(message);
	}
}
