/*
 * Copyright 2002-2004 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ez.garlick.twildemo.core.i18n;

import java.text.MessageFormat;
import java.util.Locale;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import org.springframework.beans.factory.BeanInitializationException;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.context.support.AbstractMessageSource;
import org.springframework.dao.IncorrectResultSizeDataAccessException;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcOperations;
import org.springframework.stereotype.Component;

/**
 * This class is used for resolving messages codes via jdbc. It currently only
 * support the langauge portion of the locale and defaults to ENGLISH text
 * cannot be found under the given language
 * 
 * @author stephen.garlick
 * 
 */
@Component
public class JdbcMessageSource extends AbstractMessageSource {

	@Resource(name = "namedParameterJdbcTemplate")
	private NamedParameterJdbcOperations namedParameterJdbcOperations;

	private String sqlStatement;

	private Locale defaultLocale = Locale.ENGLISH;

	@Override
	@Cacheable("referenceCache")
	protected MessageFormat resolveCode(String code, Locale locale) {
		return resolveCodeInternal(code, locale);
	}

	/**
	 * Check in base the message associated with the given code and locale
	 * 
	 * @param code
	 *            the code of the message to solve
	 * @param locale
	 *            the locale to check against
	 * @return a MessageFormat if one were found, either for the given locale or
	 *         for the default on, or null if nothing could be found
	 */
	protected MessageFormat resolveCodeInternal(String code, Locale locale) {
		String result;

		try {
			result = namedParameterJdbcOperations.getJdbcOperations()
					.queryForObject(sqlStatement,
							new Object[] { code, locale.getLanguage() },
							String.class);
		} catch (IncorrectResultSizeDataAccessException e) {
			if (locale != null) {
				// Retry with english
				try {
					result = namedParameterJdbcOperations.getJdbcOperations()
							.queryForObject(
									sqlStatement,
									new Object[] { code,
											defaultLocale.getLanguage() },
									String.class);
				} catch (IncorrectResultSizeDataAccessException ex) {
					return null;
				}
			} else {
				return null;
			}
		}

		return new MessageFormat(result, locale);
	}

	/**
	 * @param sqlStatement
	 *            The sqlStatement to set.
	 */
	public void setSqlStatement(String sqlStatement) {
		this.sqlStatement = sqlStatement;
	}

	@PostConstruct
	public void afterPropertiesSet() throws Exception {
		if (sqlStatement == null) {
			throw new BeanInitializationException(
					"Sql statement should be filled");
		}

		if (namedParameterJdbcOperations == null) {
			throw new BeanInitializationException(
					"Jdbc template should be filled");
		}

	}

	public Locale getDefaultLocale() {
		return defaultLocale;
	}

	public void setDefaultLocale(Locale defaultLocale) {
		this.defaultLocale = defaultLocale;
	}

}