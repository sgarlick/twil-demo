package ez.garlick.twildemo.core.security;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

/**
 * This class simple provides access to the static SecurityContextHolder. This
 * allows us to create and inject a bean with security information
 * 
 * @author stephen.garlick
 * 
 */
@Component
public class SecurityHolderWrapper implements SecurityHolder {

	@Override
	public Object getPrincipal() {
		return SecurityContextHolder.getContext().getAuthentication()
				.getPrincipal();
	}

	@Override
	public Authentication getAuthentication() {
		return SecurityContextHolder.getContext().getAuthentication();
	}

	@Override
	public String getName() {
		return SecurityContextHolder.getContext().getAuthentication().getName();
	}
}
