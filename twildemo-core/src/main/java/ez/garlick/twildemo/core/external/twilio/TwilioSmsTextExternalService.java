package ez.garlick.twildemo.core.external.twilio;

import java.util.List;

import com.twilio.sdk.resource.instance.Sms;

public interface TwilioSmsTextExternalService {

	public abstract String sendSmsText(String message, String to);

	public abstract List<Sms> fetchSmsTextLog();

}