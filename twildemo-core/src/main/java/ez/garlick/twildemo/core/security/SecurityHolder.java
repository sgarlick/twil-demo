package ez.garlick.twildemo.core.security;

import org.springframework.security.core.Authentication;

/**
 * This interface is a means to inject a users security information.
 * 
 * @author stephen.garlick
 * 
 */
public interface SecurityHolder {

	public abstract Object getPrincipal();

	public abstract Authentication getAuthentication();

	public abstract String getName();

}