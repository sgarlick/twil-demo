package ez.garlick.twildemo.service.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * Primary configuration for service layer beans.
 * 
 * @author stephen.garlick
 * 
 */
@Configuration
@ComponentScan("ez.garlick.twildemo.service")
public class ServiceConfig {

}
