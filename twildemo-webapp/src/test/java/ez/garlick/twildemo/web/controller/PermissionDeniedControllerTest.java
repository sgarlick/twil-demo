package ez.garlick.twildemo.web.controller;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

import org.junit.Before;
import org.junit.Test;

import ez.garlick.twildemo.web.controller.PermissionDeniedController;

public class PermissionDeniedControllerTest {
	private PermissionDeniedController controller;

	@Before
	public void setup() {
		controller = new PermissionDeniedController();
	}

	@Test
	public void view() {
		String view = controller.view(null);
		assertThat(
				"PermissionDeniedController should return error/permission-denied view",
				view, is("error/permission-denied"));
	}
}
