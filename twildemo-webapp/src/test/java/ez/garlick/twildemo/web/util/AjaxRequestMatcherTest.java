package ez.garlick.twildemo.web.util;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.mock.web.MockHttpServletRequest;

public class AjaxRequestMatcherTest {

	private AjaxRequestMatcher ajaxRequestMatcher;

	@Before
	public void setup() {
		ajaxRequestMatcher = new AjaxRequestMatcher();
	}

	@Test
	public void matches() {
		MockHttpServletRequest request = new MockHttpServletRequest();
		request.addHeader("X-Requested-With", "XMLHttpRequest");
		Assert.assertTrue(ajaxRequestMatcher.matches(request));
	}

	@Test
	public void notMatches() {
		MockHttpServletRequest request = new MockHttpServletRequest();
		Assert.assertFalse(ajaxRequestMatcher.matches(request));
	}
}
