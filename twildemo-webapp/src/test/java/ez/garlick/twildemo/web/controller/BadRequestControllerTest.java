package ez.garlick.twildemo.web.controller;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

import org.junit.Before;
import org.junit.Test;

import ez.garlick.twildemo.web.controller.BadRequestController;

public class BadRequestControllerTest {

	private BadRequestController controller;

	@Before
	public void setup() {
		controller = new BadRequestController();
	}

	@Test
	public void view() {
		String view = controller.view(null);
		assertThat("BadRequestController should return error/400 view", view,
				is("error/400"));
	}
}
