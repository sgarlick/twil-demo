package ez.garlick.twildemo.web.controller;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

import org.junit.Before;
import org.junit.Test;

import ez.garlick.twildemo.web.controller.LoginController;

public class LoginControllerTest {
	private LoginController controller;

	@Before
	public void setup() {
		controller = new LoginController();
	}

	@Test
	public void view() {
		String view = controller.view(null);
		assertThat("LoginController should return login view", view,
				is("login"));
	}
}
