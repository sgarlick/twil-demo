package ez.garlick.twildemo.web.controller;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

import org.junit.Before;
import org.junit.Test;

import ez.garlick.twildemo.web.controller.HomePageController;

public class HomePageControllerTest {
	private HomePageController controller;

	@Before
	public void setup() {
		controller = new HomePageController();
	}

	@Test
	public void view() {
		String view = controller.view(null);
		assertThat("HomePageControllershould return home view", view,
				is("home"));
	}
}
