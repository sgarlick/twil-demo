<%@include file="/WEB-INF/jsp/include.jsp"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Success</title>
<style type="text/css">
	td {
		border-style: inset;
	}
</style>
</head>
<body>
	<table>
		<thead>
			<tr>
				<th>SID</th>
				<th>Date Sent</th>
				<th>To</th>
				<th>From</th>
				<th>Status</th>
				<th>Body</th>
			</tr>
		</thead>
		<tbody>
			<c:forEach items="${sms}" var="text">
				<tr>
					<td>${text.sid}</td>
					<td>${text.dateSent}</td>
					<td>${text.to}</td>
					<td>${text.from}</td>
					<td>${text.status}</td>
					<td>${text.body}</td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
</body>
</html>