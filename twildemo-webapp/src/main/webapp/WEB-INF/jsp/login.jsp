<%@include file="/WEB-INF/jsp/include.jsp" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Login</title>
</head>
<body>
	<c:if test="${not empty param.error}">
		<c:choose>
			<c:when test="${param.error eq 2}">Your session has timed out.  Please log in again</c:when>
			<c:when test="${param.error eq 3}">Logout Successful.</c:when>			
			<c:otherwise>
				<c:out value="${SPRING_SECURITY_LAST_EXCEPTION.message}" />
			</c:otherwise>
		</c:choose>
	</c:if>
	<form method="post" action="<c:url value="/login/process" />">
		<div>
			<label for="username">Username: </label><input type="text"
				name="j_username" id="username"/>
		</div>
		<div>
			<label for="password">Password: </label><input type="password"
				name="j_password" id="password" />
		</div>
		<div>
			<input type="submit"  value="Login" />
		</div>
		<div>
		<%--	<input type='checkbox'
				name="_spring_security_remember_me" id="remember" value="true" checked="checked"/><label for="remember">Remember Me</label>  --%>
		</div>
	</form>
</body>
</html>