<%@include file="/WEB-INF/jsp/include.jsp" %>
<div>
	<div>Header</div>
	<sec:authorize access="isAuthenticated()">
		<div>
			<a href="<c:url value="/logout" />">Logout</a>
		</div>
	</sec:authorize>
</div>