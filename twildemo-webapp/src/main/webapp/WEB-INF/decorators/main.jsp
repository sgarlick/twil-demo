<%@include file="/WEB-INF/jsp/include.jsp" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">

<sec:authorize access="isAuthenticated()">
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>
	<script type="text/javascript">
		if (typeof jQuery == 'undefined') {
			document
					.write(unescape("%3Cscript src='<c:url value="/resources/js/lib/jquery.min.js"/>' type='text/javascript'%3E%3C/script%3E"));
		}
	</script>
	<script src="<c:url value="/resources/js/common.js"/>" async defer></script>
</sec:authorize>
<link rel="stylesheet"
	href="<c:url value="/resources/css/common.css"/>">

<decorator:head />
<title>Twilio Demo - <decorator:title /></title>
</head>
<body>
	<div id="header">
		<page:applyDecorator name="header" />
	</div>
	<hr>
	<div id="content">
		<decorator:body />
	</div>
	<hr>
	<div id="footer">
		<page:applyDecorator name="footer" />
	</div>
</body>
</html>