package ez.garlick.twildemo.web.controller;

import javax.inject.Inject;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import ez.garlick.twildemo.core.external.twilio.TwilioSmsTextExternalService;

/**
 * Our initial page after logging in.
 * 
 * @author stephen.garlick
 * 
 */
@Controller
@RequestMapping(value = "/")
public class HomePageController {
	@Inject
	private TwilioSmsTextExternalService twilioSmsTextExternalService;
	
	@RequestMapping(method = RequestMethod.GET)
	public String view(ModelMap modelMap) {
		//lets just send a text when they hit ot home page to test it out.
		//twilioSmsTextExternalService.sendSmsText("Hideho", "+15712752685");
		modelMap.put("sms", twilioSmsTextExternalService.fetchSmsTextLog());
		
		
		return "home";
	}
}
