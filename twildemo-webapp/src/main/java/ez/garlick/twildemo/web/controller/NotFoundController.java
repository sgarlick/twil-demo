package ez.garlick.twildemo.web.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * This class maps http statsu 404 errors
 * 
 * @author stephen.garlick
 * 
 */
@Controller
@RequestMapping(value = "error/404")
public class NotFoundController {
	@RequestMapping(method = RequestMethod.GET)
	public String view(ModelMap modelMap) {
		return "error/404";
	}
}
