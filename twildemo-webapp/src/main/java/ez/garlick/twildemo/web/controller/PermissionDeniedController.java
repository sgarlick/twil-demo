package ez.garlick.twildemo.web.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * This controller handles the view for when a user tries to access a protected
 * resource.
 * 
 * @author stephen.garlick
 * 
 */
@Controller
@RequestMapping(value = "error/permission-denied")
public class PermissionDeniedController {

	@RequestMapping(method = RequestMethod.GET)
	public String view(ModelMap modelMap) {
		return "error/permission-denied";
	}
}
