package ez.garlick.twildemo.web.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * This controller maps http status 400 errors
 * 
 * @author stephen.garlick
 * 
 */
@Controller
@RequestMapping(value = "error/400")
public class BadRequestController {
	@RequestMapping(method = RequestMethod.GET)
	public String view(ModelMap modelMap) {
		return "error/400";
	}
}
