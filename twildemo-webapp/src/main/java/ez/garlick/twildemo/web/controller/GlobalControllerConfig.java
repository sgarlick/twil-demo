package ez.garlick.twildemo.web.controller;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.InitBinder;

/**
 * This is a class that applies controller annotations globally. See
 * {@link InitBinder} {@link ExceptionHandler} etc
 * 
 * @author stephen.garlick
 * 
 */
@ControllerAdvice
public class GlobalControllerConfig {

}
