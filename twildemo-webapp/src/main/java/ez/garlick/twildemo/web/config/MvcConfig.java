package ez.garlick.twildemo.web.config;

import java.util.Locale;

import org.springframework.beans.factory.BeanInitializationException;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.Profile;
import org.springframework.validation.Validator;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;
import org.springframework.web.servlet.HandlerExceptionResolver;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.handler.SimpleMappingExceptionResolver;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.JstlView;

import ez.garlick.twildemo.core.config.AspectJConfig;
import ez.garlick.twildemo.core.config.CacheConfig;
import ez.garlick.twildemo.core.config.DatabaseConfig;
import ez.garlick.twildemo.core.config.ExternalServicesConfig;
import ez.garlick.twildemo.core.config.SecurityConfig;
import ez.garlick.twildemo.core.config.TwilioConfig;
import ez.garlick.twildemo.core.i18n.JdbcMessageSource;
import ez.garlick.twildemo.service.config.ServiceConfig;
import ez.garlick.twildemo.web.util.AjaxRequestMatcher;

/**
 * Main configuration for Spring MVC web projects. Imports several other
 * configurations needed for a project.
 * 
 * @author stephen.garlick
 * 
 */
@Configuration
@EnableWebMvc
@Import({ AspectJConfig.class, DatabaseConfig.class, ServiceConfig.class,
		SecurityConfig.class, CacheConfig.class, ExternalServicesConfig.class, TwilioConfig.class })
@ComponentScan(basePackages = "ez.garlick.twildemo.web.controller")
public class MvcConfig extends WebMvcConfigurerAdapter {

	/**
	 * Used to resolve jsps. When a controller returns a String, this class will
	 * resolve the jsp with the configured settings.
	 * 
	 * @return
	 */
	@Bean
	public ViewResolver viewResolver() {
		InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
		viewResolver.setViewClass(JstlView.class);
		viewResolver.setPrefix("/WEB-INF/jsp/");
		viewResolver.setSuffix(".jsp");
		return viewResolver;
	}



	/**
	 * Here we map our static resources and set the cache time to the maximum.
	 */
	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		registry.addResourceHandler("/resources/**")
				.addResourceLocations("/WEB-INF/static/")
				.setCachePeriod(31556926);
	}

	/**
	 * Registering our validator with spring mvc for our i18n support
	 */
	@Override
	public Validator getValidator() {
		try {
			return validator();
		} catch (Exception e) {
			throw new BeanInitializationException(
					"exception when registering validator in "
							+ this.getClass().getName(), e);
		}
	}

	/**
	 * Our spring message source bean used for localization. Currently
	 * configured to support a single language parameter.
	 * 
	 * @return
	 * @throws Exception
	 */
	@Bean
	public MessageSource messageSource() throws Exception {
		JdbcMessageSource messageSource = new JdbcMessageSource();
		messageSource.setUseCodeAsDefaultMessage(true);
		messageSource
				.setSqlStatement("select text from ref_resource_bundle where text_id = ? and language = ?");
		messageSource.setDefaultLocale(Locale.ENGLISH);
		return messageSource;
	}
	
	/**
	 * Match request with ajax headers.
	 * @return
	 */
	@Bean
	public AjaxRequestMatcher ajaxRequestMatcher()
	{
		return new AjaxRequestMatcher();
	}

	/**
	 * This dev profile will show a default error page including exception
	 * information.
	 * 
	 * @author stephen.garlick
	 * 
	 */
	@Profile("dev")
	@Configuration
	static class DevMvcConfig {
		@Bean
		public HandlerExceptionResolver simpleMappingExceptionResolver() {
			SimpleMappingExceptionResolver resolver = new SimpleMappingExceptionResolver();
			resolver.setDefaultErrorView("error/default-error");
			resolver.setDefaultStatusCode(500);
			return resolver;
		}
	}

	/**
	 * This prod profile will show a default error page with no exception
	 * information.
	 * 
	 * @author stephen.garlick
	 * 
	 */
	@Profile("prod")
	@Configuration
	static class ProdMvcConfig {
		@Bean
		public HandlerExceptionResolver simpleMappingExceptionResolver() {
			SimpleMappingExceptionResolver resolver = new SimpleMappingExceptionResolver();
			resolver.setDefaultErrorView("error/hidden-error");
			resolver.setDefaultStatusCode(500);
			return resolver;
		}
	}

	/**
	 * This is the validator we will provide to spring mvc to handle message
	 * translation for the bean validation api (hibernate-validation)
	 * 
	 * @return
	 * @throws Exception
	 */
	@Bean
	public LocalValidatorFactoryBean validator() throws Exception {
		LocalValidatorFactoryBean bean = new LocalValidatorFactoryBean();
		bean.setValidationMessageSource(messageSource());
		return bean;
	}
}