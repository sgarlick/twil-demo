package ez.garlick.twildemo.web.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * This class maps http status 500 errors
 * 
 * @author stephen.garlick
 * 
 */
@Controller
@RequestMapping(value = "error/500")
public class InternalServerErrorController {

	
	@RequestMapping(method = RequestMethod.GET)
	public String view(ModelMap modelMap) {
		return "error/500";
	}
}
