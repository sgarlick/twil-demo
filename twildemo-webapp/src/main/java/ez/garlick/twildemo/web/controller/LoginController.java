package ez.garlick.twildemo.web.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Controller that handles the view for our login page.
 * 
 * @author stephen.garlick
 * 
 */
@Controller
@RequestMapping(value = "login")
public class LoginController {

	@RequestMapping(method = RequestMethod.GET)
	public String view(ModelMap modelMap) {
		return "login";
	}
}
