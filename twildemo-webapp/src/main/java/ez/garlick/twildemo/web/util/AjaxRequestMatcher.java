package ez.garlick.twildemo.web.util;

import javax.servlet.http.HttpServletRequest;

import org.springframework.security.web.util.RequestMatcher;
import org.springframework.stereotype.Component;

/**
 * This class is intended to be used to match request made via ajax.
 * 
 * @author stephen.garlick
 * 
 */
@Component
public class AjaxRequestMatcher implements RequestMatcher {

	@Override
	public boolean matches(HttpServletRequest request) {
		if ("XMLHttpRequest".equals(request.getHeader("X-Requested-With")))
			return true;
		return false;
	}

}
